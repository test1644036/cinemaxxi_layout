* Programming Language: Kotlin
* Build System: IntelliJ IDEA
* JDK: 17

# Persyaratan
>1. Dibutuhkan code editor IntelliJ IDEA
>2. Dibutuhkan Java Development Kit 17

# Cara Menjalankan Aplikasi
>1. Buka code editor IntelliJ IDEA
>2. Buka project cinemaxxi_layout
>3. Cari file Main.kt
>4. Klik kanan Run 'MainKt'

# Cara Menggunakan Aplikasi
>1. Ketika aplikasi dijalankan akan diarahkan untuk mengisi konfigurasi denah studio
>2. Masukan label kursi: A (Alphanumeric)
>3. Masukan jumlah kursi: 5 (Numeric Integer)
>4. Setelah konfigurasi berhasil akan diarahkan ke pilihan menu
>5. Masukkan pilihan menu: A atau B atau C atau D atau Exit (Case Sensitive)


![](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)
![img_3.png](img_3.png)
![img_4.png](img_4.png)
![img_5.png](img_5.png)
![img_6.png](img_6.png)
![img_7.png](img_7.png)