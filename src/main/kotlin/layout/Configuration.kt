package layout

import dataCacheConfig
import helper.generatePrettyString
import model.SeatsLayout
import model.SeatsStatus

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/
fun showLayoutConfiguration() {
    println(generatePrettyString("Selamat Datang (Cinema XXI)"))
    println(generatePrettyString("Silahkan masukkan konfigurasi denah studio"))
    println()

    val inputSeatsLabel = inputSeatsLabel()
    val inputQtyLabel = inputQtyLabel()
    generateSeats(inputSeatsLabel, inputQtyLabel)

    println()
    println(generatePrettyString("Aplikasi Cinema XXI Layout (kursi tersedia $inputSeatsLabel-$inputQtyLabel)"))
    println()
}

private fun inputSeatsLabel(): String {
    print("Masukkan label kursi: ")
    val input = readLine()
    return if(!input.isNullOrEmpty()) {
        input
    } else {
        println("Silahkan masukan label kursi terlebih dahulu!")
        inputSeatsLabel()
    }
}

private fun inputQtyLabel(): Int {
    print("Masukkan jumlah kursi: ")
    val input = readLine()
    return if(!input.isNullOrEmpty() && input.toIntOrNull() != null) {
        val inputQty = input.toInt()
        val seatsLimit = 20
        if(inputQty <= seatsLimit) {
            inputQty
        } else {
            println("Jumlah kursi tidak boleh lebih dari 20!")
            inputQtyLabel()
        }
    } else {
        println("Silahkan masukan jumlah kursi (angka 1-20) terlebih dahulu!")
        inputQtyLabel()
    }
}

private fun generateSeats(inputSeatsLabel: String, inputQtyLabel: Int) {
    val generatedSeats = MutableList(inputQtyLabel) { index ->
        SeatsLayout(inputSeatsLabel + (index + 1), SeatsStatus.Free.name)
    }
    dataCacheConfig.addAll(generatedSeats)
}