package layout

import dataCacheConfig
import helper.generatePrettyString
import inputEnter

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/
fun showLayoutSeatsStatus() {
    println(generatePrettyString("Denah Status"))
    println()

    dataCacheConfig.forEach {
        println("${it.seatsCode}-${it.seatsStatus}")
    }

    println()
    inputEnter()
    println()
}