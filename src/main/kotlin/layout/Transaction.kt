package layout

import dataCacheConfig
import helper.generatePrettyString
import inputEnter
import model.SeatsStatus
import java.time.format.DateTimeFormatter

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/
fun showLayoutTransactionStatus() {
    println(generatePrettyString("Denah Transaction Status"))
    println()

    val statusSummary = getSeatsStatusSummary()
    println("Total ${statusSummary["totalFree"]} Free, ${statusSummary["totalSold"]} Sold")
    println()

    val formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss")
    dataCacheConfig.forEach {
        if(it.seatsStatus == SeatsStatus.Sold.name) println("${it.seatsCode}-${it.transactionTime!!.format(formatter)}")
    }

    println()
    inputEnter()
    println()
}

private fun getSeatsStatusSummary(): Map<String, Int> {
    var totalFree = 0
    var totalSold = 0

    dataCacheConfig.forEach { seatsLayout ->
        if(seatsLayout.seatsStatus == SeatsStatus.Free.name) totalFree += 1 else totalSold += 1
    }

    return mapOf(
        "totalFree" to totalFree,
        "totalSold" to totalSold
    )
}