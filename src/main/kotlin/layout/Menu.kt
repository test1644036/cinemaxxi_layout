package layout

import helper.generatePrettyString
import kotlin.system.exitProcess

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/
fun showLayoutMenu() {
    val menu = listOf(
        "A" to "A) Pemesanan Kursi",
        "B" to "B) Batalkan Kursi",
        "C" to "C) Laporan Denah",
        "D" to "D) Laporan Transaksi",
    )

    println(generatePrettyString("Pilih salah satu menu di bawah ini"))
    menu.forEach {
        println(it.second)
    }
    println()
    println("Masukkan 'Exit' untuk keluar.")
    println()

    when(inputMenu()) {
        "A" -> { showLayoutBookingSeats() }
        "B" -> { showLayoutCancelSeats() }
        "C" -> { showLayoutSeatsStatus() }
        "D" -> { showLayoutTransactionStatus() }
        "Exit" -> { exitProcess(1) }
        else -> {
            showLayoutMenu()
        }
    }
}

private fun inputMenu(): String {
    print("Masukkan: ")
    val input = readLine()
    return if(!input.isNullOrEmpty()) {
        input
    } else {
        println("Silahkan pilih menu terlebih dahulu!")
        inputMenu()
    }
}