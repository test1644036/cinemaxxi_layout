package layout

import dataCacheConfig
import helper.generatePrettyString
import inputEnter
import model.SeatsStatus
import java.time.LocalDateTime

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/
fun showLayoutBookingSeats() {
    println(generatePrettyString("Pemesanan Kursi"))
    println()

    val inputSeatsCode = inputSeatsCode()
    val result = bookingSeats(inputSeatsCode)

    println()
    println(result)
    inputEnter()
    println()
}

private fun inputSeatsCode(): String {
    print("Masukkan kode kursi: ")
    val input = readLine()
    return if(!input.isNullOrEmpty()) {
        input
    } else {
        println("Silahkan masukan kode kursi terlebih dahulu!")
        inputSeatsCode()
    }
}

private fun bookingSeats(inputSeatsCode: String): String {
    val seats = dataCacheConfig.filter { it.seatsCode == inputSeatsCode }
    return if(seats.isNotEmpty()) {
        if(seats[0].seatsStatus == SeatsStatus.Free.name) {
            seats[0].seatsStatus = SeatsStatus.Sold.name
            seats[0].transactionTime = LocalDateTime.now()
            "Kursi $inputSeatsCode berhasil di pesan."
        } else {
            "Status kursi $inputSeatsCode sudah ${SeatsStatus.Sold.name}!"
        }
    } else {
        "Kursi $inputSeatsCode belum di konfigurasi!"
    }
}