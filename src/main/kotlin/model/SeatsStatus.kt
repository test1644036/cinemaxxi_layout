package model

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/
enum class SeatsStatus {
    Free, Sold
}