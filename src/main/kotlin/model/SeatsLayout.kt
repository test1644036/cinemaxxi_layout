package model

import java.time.LocalDateTime

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/
data class SeatsLayout(
    var seatsCode: String,
    var seatsStatus: String,
    var transactionTime: LocalDateTime? = null
)