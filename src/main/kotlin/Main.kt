import layout.showLayoutConfiguration
import layout.showLayoutMenu
import model.SeatsLayout

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/

val dataCacheConfig = mutableListOf<SeatsLayout>()

fun main() {
    showLayoutConfiguration()
    showLayoutMenu()
}

fun inputEnter() {
    print("Tekan 'Enter' untuk kembali ke menu.")
    readLine()
    showLayoutMenu()
}