package helper

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 09/09/2023, Saturday
 **/
fun generatePrettyString(message: String): String {
    val maxLength = 80
    val messageLength = message.length
    val prefixLength = (maxLength - messageLength) / 2
    val suffixLength = maxLength - messageLength - prefixLength
    val customString = '='
    val prefixString = "".padStart(prefixLength, customString)
    val suffixString = "".padEnd(suffixLength, customString)
    return prefixString + message + suffixString
}